#!/bin/bash
# - Setup a local DV dev folder
# - Copy there isolation Tuple Tools and compile them
PLATFORM=x86_64-slc6-gcc49-opt
DVVER=v42r3
DVFOLDER=$HOME # Where you wish your DV dev to live
DVDEV=${DVFOLDER}/DaVinciDev_${DVVER}
ISODEVDIR=$PWD

LbLogin -c $PLATFORM && \
cd $DVFOLDER
# Either a DV dev folder exists or make one
[[ -e `basename ${DVDEV}` || $(lb-dev DaVinci $DVVER) ]] && \
cd DaVinciDev_${DVVER}/ && \
git lb-use Analysis && \
git lb-checkout Analysis/master Phys/DecayTreeTuple && \
cp ${ISODEVDIR}/src/* ${DVDEV}/Phys/DecayTreeTuple/src/ && \
lb-run DaVinci/${DVVER} make -j8 -C $DVDEV configure && \
lb-run DaVinci/${DVVER} make -j8 -C $DVDEV install

