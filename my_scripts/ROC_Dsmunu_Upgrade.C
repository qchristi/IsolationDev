using namespace std;

void ROC_Dsmunu_Upgrade()
{
    // Get official singal trees
  TChain chain("myTree");
    chain.Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_JUNE19/DTT_13512010_Bs_Kmunu_DecProdCut_Up_Py8_MC12.root/Bs2KmuNuTuple/DecayTree");
    chain.Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_JUNE19/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12.root/Bs2KmuNuTuple/DecayTree");
    cout << "Kmunu official signal acquired" << endl;

    //Get upgrade signal trees
    TFile* file = TFile::Open("/eos/lhcb/user/b/bkhanji/ForChristian/AllPVs/DTT_13512010_Upgrade.root");
    TDirectoryFile *cart = (TDirectoryFile*) file->Get("Bs2KmuNuTuple");
    TTree* t = (TTree*) cart->Get("DecayTree");
    cout << "Kmunu upgrade signal acquired" << endl;

    //Get official Dsmunu trees
    TChain chain2("myTree2");
    chain2.Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_JUNE19/DTT_13774000_Bs_Dsmunu_cocktail_hqet2_DsmuInAcc_Up_Py8_MC12.root/Bs2KmuNuTuple/DecayTree");
    chain2.Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_JUNE19/DTT_13774000_Bs_Dsmunu_cocktail_hqet2_DsmuInAcc_Dn_Py8_MC12.root/Bs2KmuNuTuple/DecayTree");
     cout << "Dsmunu official background acquired" << endl;

    //Get upgrade Dsmunu trees
    TFile* file2 = TFile::Open("/eos/lhcb/user/b/bkhanji/ForChristian/AllPVs/DTT_13774002_Upgrade.root");
    TDirectoryFile *cart2 = (TDirectoryFile*) file2->Get("Bs2KmuNuTuple");
    TTree* t2 = (TTree*) cart2->Get("DecayTree");
     cout << "Dsmunu upgrade background acquired" << endl;
    
    //Define RDataFrame for each tree
    auto sig_off_Kmunu = ROOT::RDataFrame(chain);
    auto bkg_off_Dsmunu = ROOT::RDataFrame(chain2);
    auto sig_up_Kmunu = ROOT::RDataFrame(*t);
    auto bkg_up_Dsmunu = ROOT::RDataFrame(*t2);
    cout << "RDataFrames constructed" << endl;
    
    auto ent_off_Kmunu = sig_off_Kmunu.Count();
    auto ent_off_Dsmunu = bkg_off_Dsmunu.Count();
    auto ent_up_Kmunu = sig_up_Kmunu.Count();
    auto ent_up_Dsmunu = bkg_up_Dsmunu.Count();
    cout << "N° events in Kmunu official signal:        " << *ent_off_Kmunu << endl;
    cout << "N° events in Kmunu upgrade signal:         " << *ent_up_Kmunu << endl;
    cout << "N° events in Dsmunu official background:   " << *ent_off_Dsmunu << endl;
    cout << "N° events in Dsmunu upgrade background:    " << *ent_up_Dsmunu << endl;
    
    TGraphErrors* ROC_official = new TGraphErrors();
    TGraphErrors* ROC_upgrade = new TGraphErrors();
    int i = 1;
    double sig_eff, sig_eff_err, bkg_eff, bkg_eff_err;
    cout << "going into the cycle.." << endl;
    for (double cut = -0.98; cut < 1; cut+= .02)
    {
        //Count how many (sig & bkg) events pass the cut on IsoBDT variable
        auto IsoCut = [=](double x) {return x > cut;};
        auto N_sig_off_Kmunu = sig_off_Kmunu.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto N_bkg_off_Dsmunu = bkg_off_Dsmunu.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto N_sig_up_Kmunu = sig_up_Kmunu.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto N_bkg_up_Dsmunu = bkg_up_Dsmunu.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
	
        //ROC for official Kmunu/Dsmunu
        sig_eff = *N_sig_off_Kmunu/(1.*(*ent_off_Kmunu));
        bkg_eff = *N_bkg_off_Dsmunu/(1.*(*ent_off_Dsmunu));

        sig_eff_err = sqrt((*N_sig_off_Kmunu + 1.)*(*N_sig_off_Kmunu + 2.)/(1.*(*ent_off_Kmunu + 2.)*(*ent_off_Kmunu + 3.)) - pow(*N_sig_off_Kmunu + 1., 2)/(1.*pow(*ent_off_Kmunu + 2., 2)));
        bkg_eff_err = sqrt((*N_bkg_off_Dsmunu + 1.)*(*N_bkg_off_Dsmunu + 2.)/(1.*(*ent_off_Dsmunu + 2.)*(*ent_off_Dsmunu + 3.)) - pow(*N_bkg_off_Dsmunu + 1., 2)/(1.*pow(*ent_off_Dsmunu + 2., 2)));

        ROC_official->SetPoint(i, sig_eff, 1. - bkg_eff);
        ROC_official->SetPointError(i, sig_eff_err, bkg_eff_err);

        //ROC for upgrade Kmunu/Dsmunu
        sig_eff = *N_sig_up_Kmunu/(1.*(*ent_up_Kmunu));
        bkg_eff = *N_bkg_up_Dsmunu/(1.*(*ent_up_Dsmunu));

        sig_eff_err = sqrt((*N_sig_up_Kmunu + 1.)*(*N_sig_up_Kmunu + 2.)/(1.*(*ent_up_Kmunu + 2.)*(*ent_up_Kmunu + 3.)) - pow(*N_sig_up_Kmunu + 1., 2)/(1.*pow(*ent_up_Kmunu + 2., 2)));
        bkg_eff_err = sqrt((*N_bkg_up_Dsmunu + 1.)*(*N_bkg_up_Dsmunu + 2.)/(1.*(*ent_up_Dsmunu + 2.)*(*ent_up_Dsmunu + 3.)) - pow(*N_bkg_up_Dsmunu + 1., 2)/(1.*pow(*ent_up_Dsmunu + 2., 2)));

        ROC_upgrade->SetPoint(i, sig_eff, 1. - bkg_eff);
        ROC_upgrade->SetPointError(i, sig_eff_err, bkg_eff_err);

        i++;
    }
    cout << "straight outta cycle" << endl;
    ROC_official->SetTitle("ROC_D_{s}#mu#nu_Official_vs_Upgrade;Sig_eff;Bkg_rej;");
    ROC_upgrade->SetTitle("ROC_Dsmunu_vs_JPsiK+");
    ROC_official->SetMarkerStyle(7);
    ROC_official->SetMarkerColor(kRed);
    ROC_upgrade->SetMarkerStyle(7);
    ROC_upgrade->SetMarkerColor(kBlue);
    
    TCanvas* c;
    ROC_official->Draw("AP");
    ROC_upgrade->Draw("P");
    auto legend = new TLegend(0.57,0.7,0.88,0.85);
    legend->AddEntry(ROC_official,"Official_samples", "lp");
    legend->AddEntry(ROC_upgrade,"Upgrade_samples", "lp");
    legend->Draw();
}
