using namespace std;

void ROC_Curve_Dsmunu_Upgrade()
{
    // Get official singal trees
    TFile* file = TFile::Open("/eos/lhcb/user/b/bkhanji/ForChristian/AllPVs/DTT_13512010_Upgrade.root");
    TDirectoryFile *cart = (TDirectoryFile*) file->Get("Bs2KmuNuTuple");
    TTree* t = (TTree*) cart->Get("DecayTree");
    cout << "Official signal acquired" << endl;

    //Get official bkg trees
    TFile* file2 = TFile::Open("/eos/lhcb/user/b/bkhanji/ForChristian/AllPVs/DTT_13774002_Upgrade.root");
    TDirectoryFile *cart2 = (TDirectoryFile*) file2->Get("Bs2KmuNuTuple");
    TTree* t2 = (TTree*) cart2->Get("DecayTree");
     cout << "Official background acquired" << endl;
    
    //Define RDataFrame for each tree
    auto rdf_sig_off = ROOT::RDataFrame(*t);
    auto rdf_bkg_off = ROOT::RDataFrame(*t2);
    auto rdf_sig_off_PV1 = rdf_sig_off.Filter("nPVs == 1");
    auto rdf_bkg_off_PV1 = rdf_bkg_off.Filter("nPVs == 1");
    auto rdf_sig_off_PV2 = rdf_sig_off.Filter("nPVs == 2");
    auto rdf_bkg_off_PV2 = rdf_bkg_off.Filter("nPVs == 2");
    auto rdf_sig_off_PVs = rdf_sig_off.Filter("nPVs > 2");
    auto rdf_bkg_off_PVs = rdf_bkg_off.Filter("nPVs > 2");
    cout << "RDataFrames constructed" << endl;
    
    auto entsoff = rdf_sig_off.Count();
    auto entboff = rdf_bkg_off.Count();
    auto entsoff1 = rdf_sig_off_PV1.Count();
    auto entboff1 = rdf_bkg_off_PV1.Count();
    auto entsoff2 = rdf_sig_off_PV2.Count();
    auto entboff2 = rdf_bkg_off_PV2.Count();
    auto entsoffs = rdf_sig_off_PVs.Count();
    auto entboffs = rdf_bkg_off_PVs.Count();
    
    cout << "N° events in official signal:               " << *entsoff << endl;
    cout << "N° events in official background:           " << *entboff << endl;
    cout << "N° events in official signal(PV = 1):       " << *entsoff1 << endl;
    cout << "N° events in official background(PV = 1):   " << *entboff1 << endl;
    cout << "N° events in official signal(PV = 2):       " << *entsoff2 << endl;
    cout << "N° events in official background(PV = 2):   " << *entboff2 << endl;
    cout << "N° events in official signal(PV > 2):       " << *entsoffs << endl;
    cout << "N° events in official background(PV > 2):   " << *entboffs << endl;
    
    TGraphErrors* offic = new TGraphErrors();
    TGraphErrors* offic1 = new TGraphErrors();
    TGraphErrors* offic2 = new TGraphErrors();
    TGraphErrors* offics = new TGraphErrors();
    int i = 1;
    double sig_eff, sig_eff_err, bkg_eff, bkg_eff_err;
    cout << "going into the cycle.." << endl;
    for (double cut = -0.98; cut < 1; cut+= .02)
    {
        //Count how many (sig & bkg) events pass the cut on IsoBDT variable
        auto IsoCut = [=](double x) {return x > cut;};
        auto sig_o = rdf_sig_off.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto bkg_o = rdf_bkg_off.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto sig_o1 = rdf_sig_off_PV1.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto bkg_o1 = rdf_bkg_off_PV1.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto sig_o2 = rdf_sig_off_PV2.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto bkg_o2 = rdf_bkg_off_PV2.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto sig_os = rdf_sig_off_PVs.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();
        auto bkg_os = rdf_bkg_off_PVs.Filter(IsoCut, {"muon_p_IsoMinBDT"}).Count();

        //ROC for official files
        sig_eff = *sig_o/(1.*(*entsoff));
        bkg_eff = *bkg_o/(1.*(*entboff));

        sig_eff_err = sqrt((*sig_o + 1.)*(*sig_o + 2.)/(1.*(*entsoff + 2.)*(*entsoff + 3.)) - pow(*sig_o + 1., 2)/(1.*pow(*entsoff + 2., 2)));
        bkg_eff_err = sqrt((*bkg_o + 1.)*(*bkg_o + 2.)/(1.*(*entboff + 2.)*(*entboff + 3.)) - pow(*bkg_o + 1., 2)/(1.*pow(*entboff + 2., 2)));

        offic->SetPoint(i, sig_eff, 1. - bkg_eff);
        offic->SetPointError(i, sig_eff_err, bkg_eff_err);
        
        //PV = 1
        sig_eff = *sig_o1/(1.*(*entsoff1));
        bkg_eff = *bkg_o1/(1.*(*entboff1));

        sig_eff_err = sqrt((*sig_o1 + 1.)*(*sig_o1 + 2.)/(1.*(*entsoff1 + 2.)*(*entsoff1 + 3.)) - pow(*sig_o1 + 1., 2)/(1.*pow(*entsoff1 + 2., 2)));
        bkg_eff_err = sqrt((*bkg_o1 + 1.)*(*bkg_o1 + 2.)/(1.*(*entboff1 + 2.)*(*entboff1 + 3.)) - pow(*bkg_o1 + 1., 2)/(1.*pow(*entboff1 + 2., 2)));

        offic1->SetPoint(i, sig_eff, 1. - bkg_eff);
        offic1->SetPointError(i, sig_eff_err, bkg_eff_err);
        
        //PV = 2
        sig_eff = *sig_o2/(1.*(*entsoff2));
        bkg_eff = *bkg_o2/(1.*(*entboff2));

        sig_eff_err = sqrt((*sig_o2 + 1.)*(*sig_o2 + 2.)/(1.*(*entsoff2 + 2.)*(*entsoff2 + 3.)) - pow(*sig_o2 + 1., 2)/(1.*pow(*entsoff2 + 2., 2)));
        bkg_eff_err = sqrt((*bkg_o2 + 1.)*(*bkg_o2 + 2.)/(1.*(*entboff2 + 2.)*(*entboff2 + 3.)) - pow(*bkg_o2 + 1., 2)/(1.*pow(*entboff2 + 2., 2)));
        
        offic2->SetPoint(i, sig_eff, 1. - bkg_eff);
        offic2->SetPointError(i, sig_eff_err, bkg_eff_err);
        
        //PV > 2
        sig_eff = *sig_os/(1.*(*entsoffs));
        bkg_eff = *bkg_os/(1.*(*entboffs));

        sig_eff_err = sqrt((*sig_os + 1.)*(*sig_os + 2.)/(1.*(*entsoffs + 2.)*(*entsoffs + 3.)) - pow(*sig_os + 1., 2)/(1.*pow(*entsoffs + 2., 2)));
        bkg_eff_err = sqrt((*bkg_os + 1.)*(*bkg_os + 2.)/(1.*(*entboffs + 2.)*(*entboffs + 3.)) - pow(*bkg_os + 1., 2)/(1.*pow(*entboffs + 2., 2)));

        offics->SetPoint(i, sig_eff, 1. - bkg_eff);
        offics->SetPointError(i, sig_eff_err, bkg_eff_err);

        i++;
    }
    cout << "straight outta cycle" << endl;
    offic->SetTitle("ROC_Dsmunu;Sig_eff;Bkg_rej;");
    offic1->SetTitle("ROC_Dsmunu_PV=1");
    offic2->SetTitle("ROC_Dsmunu_PV=2");
    offics->SetTitle("ROC_Dsmunu_PV>2");
    offic1->SetMarkerStyle(7);
    offic1->SetMarkerColor(kRed);
    offic->SetMarkerStyle(7);
    offic->SetMarkerColor(kBlue);
    offic2->SetMarkerStyle(7);
    offic2->SetMarkerColor(kGreen);
    offics->SetMarkerStyle(7);
    offics->SetMarkerColor(kOrange);
    
    TCanvas* c;
    offic->Draw("AP");
    offic1->Draw("P");
    offic2->Draw("P");
    offics->Draw("P");
    auto legend = new TLegend(0.57,0.7,0.88,0.85);
    legend->AddEntry(offic,"Dsmunu_Upgrade", "lp");
    legend->AddEntry(offic1,"Dsmunu_Upgrade_PV=1", "lp");
    legend->AddEntry(offic2,"Dsmunu_Upgrade_PV=2", "lp");
    legend->AddEntry(offics,"Dsmunu_Upgrade_PV>2", "lp");
    legend->Draw();
}
