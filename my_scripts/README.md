# Useful scripts:
- **Compare_Keys.C**: takes a TTree, it reads all the events and prints to std output the hierarchy of the events compatible with signal/background topology.
- **Keys_PT_Distribution.C**: takes a TTree, it reads all the events and displays the comparison between the distributions of the PT for events compatible with signal (key = no match) and events compatible with background (key = match)
- **PV_ROC.C**: takes two types of signal trees, two types of background trees, applies a filter on the number of PVs involved in the event to only one type and displays the ROC curve for both types of signal and background, considering the filter.
- **ROC_Curve_***: takes the official signal and background trees, calculates and displays the ROC curves for different filters on PVs
