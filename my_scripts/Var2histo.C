TH1F* Var2histo() {
    
    // TFile* file3 = TFile::Open("/eos/lhcb/user/b/bkhanji/ForChristian/AllPVs/DTT_13512010_Upgrade.root");
    // TDirectoryFile *cart3 = (TDirectoryFile*) file3->Get("Bs2KmuNuTuple");
    // TTree* t3 = (TTree*) cart3->Get("DecayTree");
    // auto rdf = ROOT::RDataFrame(*t3);
    // auto histo2 = rdf.Histo1D({"histo2", "muon_p_IsoMinBDT", 100, -1.1, 1.1}, "muon_p_IsoMinBDT");
  TFile* f = TFile::Open("TMVApp_Kmunu.root");
  TH1F* histo2 = (TH1F*)f->Get("my2");
  histo2->SetTitle("Kmunu");
    histo2->SetFillColor(kBlue);
    histo2->SetFillStyle(3354);
    double ent2 = histo2->GetEntries();
    histo2->Scale(1/ent2);
    return histo2;
}
